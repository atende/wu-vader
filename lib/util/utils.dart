part of vader_ui;
/**
 * Emulation of Java Enum class.
 * source: http://www.dartwebtoolkit.com/
 *
 * Example:
 *
 * class Meter<int> extends Enum<int> {
 *
 *  const Meter(int val) : super (val);
 *
 *  static const Meter HIGH = const Meter(100);
 *  static const Meter MIDDLE = const Meter(50);
 *  static const Meter LOW = const Meter(10);
 * }
 *
 * and usage:
 *
 * assert (Meter.HIGH, 100);
 * assert (Meter.HIGH is Meter);
 */
abstract class Enum<T> {

  final T _value;

  const Enum(this._value);

  T get value => _value;
}

class Status extends Enum<String>{
  const Status(String val): super(val);
  static const Status INFO = const Status('info');
  static const Status WARNING = const Status('warning');
  static const Status ERROR = const Status('error');
  static const Status SUCCESS = const Status('success');
}

class Position extends Enum<String> {
  const Position(String val): super(val);
  static const Position TOP_LEFT = const Position('top-left');
  static const Position TOP_CENTER = const Position('top-center');
  static const Position TOP_RIGHT = const Position('top-right');

  static const Position BOTTOM_LEFT = const Position('bottom-left');
  static const Position BOTTOM_CENTER = const Position('bottom-center');
  static const Position BOTTOM_RIGHT = const Position('bottom-right');
  bool isTop(){
    if(Position.TOP_CENTER == this || Position.TOP_LEFT == this || Position.TOP_RIGHT == this){
      return true;
    }else {
      return false;
    }
  }
  bool isBottom(){
    return !isTop();
  }
}