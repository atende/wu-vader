library vader_ui;
import 'package:angular/angular.dart';
import 'package:logging/logging.dart';
import 'dart:async';
import 'dart:html';
@MirrorsUsed(targets: const[
    'vader'
], override: '*')
import 'dart:mirrors';

// Components
import 'components/vader_component.dart';

// Implementations
import 'src/ui/notification_impl.dart';
import 'src/http/http_interceptors_impl.dart';
import 'src/ui/dialog_impl.dart';

part 'util/utils.dart';

// Expose APIS
part 'api/ui/notification.dart';
part 'api/http/http_interceptors.dart';
part 'api/ui/dialog.dart';

// Setup Logger
Logger vaderLogger = new Logger('vader_library_default_logger');
Injector di;
class VaderModule extends Module {
  VaderModule(){
    bind(Notify, toValue: new NotifyImpl());
    bind(Logger, toValue: vaderLogger);
    bind(VaderHttpInterceptor, toImplementation: VaderHttpInterceptorImpl);
    install(new VaderComponentModule());
  }
}

Element compileAngularElement(dynamic elm, Scope scope){
  var compiler = di.get(Compiler);
  var directiveMap = di.get(DirectiveMap);
  Element element = compile(elm,di,compiler, scope: scope, directives: directiveMap);
  return element;
}


