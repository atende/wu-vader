library vader_ui.interceptors;
import "package:vader_ui/vader.dart" show Notify, NotifyType, VaderHttpInterceptor, Status;
import "package:logging/logging.dart";
import "dart:convert" show JSON;
import "package:di/di.dart";
/**
 * Default Implementation for VaderHttpInterceptor
 */
@Injectable()
class VaderHttpInterceptorImpl implements VaderHttpInterceptor{
  Logger log;
  Notify notify;
  VaderHttpInterceptorImpl(this.log, this.notify);

  responseError(String error, int httpStatus){
    // If status is not found show WARNING
    if(httpStatus == 404){
      notify.show(error, type: NotifyType.WARNING_MESSAGE);
      return;
    }
    // If status is other error show ERROR
    if(httpStatus >= 400){
      notify.show(error, type: NotifyType.ERROR_MESSAGE);
      return;
    }
  }
  processRequest(var request){
    bool success;
    String message;
    // Tenta extrair as informações da requisição
    extractMap(Map map){
      success = map.containsKey('success') ? map['success'] : null;
      message = map.containsKey('message') ? map['message'] : null;
    }
    if(request is Map){
      extractMap(request);
    }
    if(request is String) {
      // Tray Json Parse
      try {
        var mapRequest = JSON.decode(request);
        extractMap(mapRequest);
      }catch(e){
        log.log(Level.WARNING, "The interceptor could not parse the request, "
        + "its not a json string: Previous exception $e");
      }

    }
    // By this point if the success and message is null, there is nothing in the request that should generate messages
    // But if a message is found, show it
    if(message != null){
      if(success)
        notify.show(message);
      else
        notify.show(message, status: Status.WARNING, delay: 10000);
    }

  }
}