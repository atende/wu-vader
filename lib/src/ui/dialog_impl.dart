library vader_ui.dialog;
import "package:vader_ui/vader.dart" show Dialog, DialogFuture, DialogButtons, compileAngularElement, di;
import "package:angular/angular.dart" show Scope, NullTreeSanitizer, RootScope;
import "package:vader_ui/components/vader_component.dart";
import "dart:html";
import "dart:async";

class DialogImpl extends Dialog {
  StreamController<DialogFuture> _buttonPressed = new StreamController<DialogFuture>.broadcast();
  String _title;
  String _message;
  WindowService _windowService;
  Element _window;
  dynamic _buttons;
  Completer _cancel;
  bool _hasInput;
  bool _multiline;
  DialogImpl(this._title, this._message, [this._buttons, this._hasInput = false, this._multiline = false]){
    _cancel = new Completer();
  }
  show([bool modal = false]){
    if(_window == null){
      if(_hasInput){
        if(_multiline){
          _message += "<br /><textarea></textarea>";
        }else {
          _message += "<br /><input type='text' />";
        }
      }
      WindowOptions options = new WindowOptions(title: _title, innerHtml: _message, modal: modal);
      if(_buttons != null && _buttons is DialogButtons){
        options.buttons = _createButtons(_buttons);
      }else if(_buttons != null && _buttons is List<ButtonElement>){
        options.buttons = _buttons;
      }

      _window = _compileWindow(options);
      document.body.children.add(_window);
    }else if(!document.body.children.contains(_window)){
      document.body.children.add(_window);
    }
  }
  Element _compileWindow(WindowOptions options){
    var win = new Element.tag('v-window');
    String html = "<div class='content'>";
    if(options.innerHtml != null)
      html += options.innerHtml;
    html += "</div>";
    win.setInnerHtml(html, treeSanitizer: new NullTreeSanitizer());

    if(options.buttons != null && !options.buttons.isEmpty) {
      DivElement div = new Element.div();
      div.classes.add("buttons");
      options.buttons.forEach((b)=> div.children.add(b));
      win.children.add(div);
    }
    if(options.title != null)
      win.attributes['title'] = options.title;
    if(options.modal){
      win.attributes['modal'] = "true";
    }
    if(options.center){
      win.attributes['centered'] = "true";
    }
    if(options.collapse){
      win.attributes['collapsible'] = "true";
    }else {
      win.attributes['collapsible'] = "false";
    }
    if(options.close) {
      win.attributes['closable'] = "true";
    }else {
      win.attributes['closable'] = "false";
    }
    RootScope scope = di.get(Scope);
    var _winScope = scope.createProtoChild();
    _winScope.on('window-closed').listen((_)=> _cancel.complete());
    return compileAngularElement(win, _winScope);
  }
//  alert(String message, [String title]){
//    var ok = new ButtonElement();
//    ok.value = "OK";
//    ok.innerHtml = "OK";
//    ok.classes.add('tiny');
//    ok.onClick.listen((e){
//      e.target.parent.parent.parent.parent.remove();
//      // TODO remove this workaround when detachAware is called correctly
//      var element = querySelector("#v-modal-element");
//      if(element != null){
//        element.classes.add('hide');
//      }
//    });
//    WindowOptions op = new WindowOptions(modal: true, title: title, innerHtml: message, center: true, collapse: false,
//    close: false, buttons: [ok]);
//    _window.open(op, _scope);
//  }

//  Dialog show(String message, {String title, DialogButtons buttons}) {
//    if(_buttonPressed != null && !_buttonPressed.isClosed){
//      _buttonPressed.close();
//    }
//    _buttonPressed = new StreamController<DialogFuture>();
//    WindowOptions op = new WindowOptions(title: title, innerHtml: message, center: true, collapse: false);
//    if(buttons != null){
//      op.buttons = _createButtons(buttons);
//    }
//    _window.open(op, _scope);
//    return this;
//  }

  Stream<DialogFuture> get onButtonPressed {
    return _buttonPressed.stream;
  }
  @override
  Future get onCancel => _cancel.future;
  close(){
    var element = querySelector("#v-modal-element");
    if(element != null){
      element.classes.add('hide');
    }
    _buttonPressed.close();
    _window.remove();
  }

  _createButtons(DialogButtons buttons){
    switch(buttons) {
      case DialogButtons.OK:
        var ok = _createOKButton();
        return [ok];
        break;
      case DialogButtons.OK_CANCEL:
        var ok = _createOKButton();
        var cancel = _createCANCELButton();
        return [ok,cancel];
        break;
      case DialogButtons.YES_NO_CANCEL:
        var no = _createNOButton();
        var yes = _createYESButton();
        var cancel = _createCANCELButton();
        return [yes,no,cancel];
        break;
      case DialogButtons.YES_NO:
        var no = _createNOButton();
        var yes = _createYESButton();
        return [yes, no];
    }
  }
  
  ButtonElement _createOKButton(){
    return _createButton('OK', DialogButtons.OK);
  }

  ButtonElement _createCANCELButton() {
    return _createButton('CANCEL', DialogButtons.CANCEL);
  }

  ButtonElement _createNOButton(){
    return _createButton('NO', DialogButtons.NO);
  }

  ButtonElement _createYESButton(){
    return _createButton('YES', DialogButtons.YES);
  }

  ButtonElement _createButton(String value, DialogButtons buttons){
    var b = new ButtonElement();
    b.value = value;
    b.innerHtml = value;
    b.classes.add('tiny');
    if(_hasInput){
      b.onClick.listen((e){
        var content = querySelector(_multiline ? 'textarea' : 'input');
        _buttonPressed.add(new DialogFuture(buttons, content.value));
      });
    }else {
      b.onClick.listen((e){
        _buttonPressed.add(new DialogFuture(buttons));
      });
    }
    return b;
  }

}
