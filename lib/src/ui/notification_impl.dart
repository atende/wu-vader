library vader_ui.notification;
import 'package:vader_ui/vader.dart' show NotifyType, Position, Notify, Status;
import 'package:css_animation/css_animation.dart';
import 'dart:html';
import 'dart:async';
class NotifyImpl implements Notify {
  var _trayId = "v-notifications-tray";
  DivElement _trayRoot;
  bool _animationEnabled = true;
  Position _defaultPosition = Position.TOP_CENTER;
  @override
  void show(String message, {String title, Status status : Status.INFO, NotifyType type : NotifyType.TRAY,
  int delay: 5000, Position position }) {
    if(position == null){
      position = _defaultPosition;
    }
    _setupTrayNotifications();
    _setupTrayPosition(position);
    var div = new DivElement();
    div.classes.add('v-notification ');
    if(type == NotifyType.TRAY){
      div.classes.add('tray v-notification-tray alert-box radius');
    }else if(type == NotifyType.ERROR_MESSAGE){
      div.classes.add('error-message v-notification-error-message');
      div.id='v-notification-error-message';
    }else if(type == NotifyType.WARNING_MESSAGE){
      div.classes.add('warning-message v-notification-warning-message');
      div.id='v-notification-warning-message';
    }else if(type == NotifyType.DESKTOP){
      _showNotificationInDesktop(title, message);
      return;
    }

    if(status == Status.INFO)
        div.classes.add('info');
    else if(status == Status.WARNING)
        div.classes.add('warning');
    else if(status == Status.ERROR)
        div.classes.add('alert');
    else if(status == Status.SUCCESS)
        div.classes.add('success');

    var fmessage = """
      <div class="popupContent"><h1>${title == null ? "" : title}</h1><p>$message</p></div></div>
    """;
    div.innerHtml = fmessage;
    if(type == NotifyType.TRAY) {
      _trayRoot.children.add(div);
      // Remove a mensagem ao clicar na mensagem inteira incluindo padding
      div.onClick.listen((_) => _applyHideAnimation(div));
    }
    else if(type == NotifyType.ERROR_MESSAGE || type == NotifyType.WARNING_MESSAGE) {
      document.body.children.add(div);
      // Remove a mensagem ao clicar no conteudo da mensagem
      div.children.first.onClick.listen((_) => _applyHideAnimation(div));
    }
    // After create the notification, center the tray if is needed (calculate the width)
    if(position == Position.TOP_CENTER || position == Position.BOTTOM_CENTER){
      _calculateAndCenterHorizontal(_trayRoot);
    }else{ // Clear
      _trayRoot.style.marginLeft = "0";
    }
    _applyShowAnimation(div, position);
    if(delay > 0 && type != NotifyType.ERROR_MESSAGE){
      var duration = new Duration(milliseconds: delay);
      new Timer(duration, () => _applyHideAnimation(div));
    }
  }
  void _setupTrayNotifications(){
     if(_trayRoot == null){
       _trayRoot = new DivElement();
       _trayRoot.id = _trayId;
       document.body.children.add(_trayRoot);
     }

  }
  _applyShowAnimation(Element element, Position position){
    if(!_animationEnabled){
      return;
    }
    if(position.isTop()){
      element.classes.addAll(['animated','bounceInDown']);
    }else{
      element.classes.addAll(['animated','bounceInUp']);
    }
  }
  _applyHideAnimation(Element element, [bool remove = true]){
    if(!_animationEnabled && remove){
      element.remove();
      return;
    }
    if(_animationEnabled){
      const int duration = 500;
      var animation = new CssAnimation('opacity', 1, 0);
      if(remove){
        new Timer(const Duration(milliseconds: duration), () => element.remove());
      }

      animation.apply(element, duration: duration);
    }else{
      element.remove();
    }


  }

  void enableAnimations(bool enabled) {
    this._animationEnabled = enabled;
  }
  bool isAnimationEnabled(){
    return this._animationEnabled;
  }
  _setupTrayPosition(Position position) {
    _trayRoot.classes.clear();
    switch(position){
      case Position.TOP_RIGHT:
        _trayRoot.classes.add('v-top-right');
        break;
      case Position.TOP_LEFT:
        _trayRoot.classes.add('v-top-left');
        break;
      case Position.TOP_CENTER:
        _trayRoot.classes.add('v-top-center');
        break;
      case Position.BOTTOM_RIGHT:
        _trayRoot.classes.add('v-bottom-right');
        break;
      case Position.BOTTOM_LEFT:
        _trayRoot.classes.add('v-bottom-left');
        break;
      case Position.BOTTOM_CENTER:
        _trayRoot.classes.add('v-bottom-center');
        break;
    }
  }
  _calculateAndCenterHorizontal(Element element){
    var width = element.clientWidth;
    element.style.marginLeft = "-${width / 2}px";
  }
  void setDefaultPosition(Position position){
    this._defaultPosition = position;
  }

  /**
   * Show desktop notifications, if is not possible to show the notification (user denied or API not implemented)
   * this method will fall back to browser notifications
   */
  _showNotificationInDesktop(String title, String message){
    Notification.requestPermission().then((p){
       if(p == "granted"){
         if(title == null){
           title = "";
         }
         new Notification(title, body: message);
       }else {
         show(message, title: title, delay: 0);
       }
       if(p == "denied"){
         vaderLogger.fine("Notification in desktop is disabled, falling back to browser notifications");
       }

     });
  }
}