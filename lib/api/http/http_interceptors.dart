part of vader_ui;
/**
 * This class provide a global HttpInterceptor for Angular
 */
abstract class VaderHttpInterceptor {
  /**
   * Show notification error messages on fatal errors.
   * Parameters
   * [error:String] The String representation of the error, can contain Html
   * [httpStatus:int] Http Code Status (ex.: 404, 501, 403)
   */
  responseError(String error, int httpStatus);

  /**
   * Process the request, the request could be String or [Map].
   * This function try to see a **message** and **success** *property* in the response, and them use [Notify] class
   * to provide user feedback, the intent is provide automatic feedback based on server response.
   *
   * You can use Angular [HttpInterceptor](https://github.com/angular/angular.dart/blob/master/lib/core_dom/http.dart)
   * to provide automate this, see [Examples](http://vader.atende.info/examples)
   */
  processRequest(dynamic request);
}
