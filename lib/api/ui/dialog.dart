part of vader_ui;
abstract class Dialog {
  /**
   * Show an modal alert box
   */
  static Dialog alert(String message, [String title]){
    var d = new DialogImpl(title, message, DialogButtons.OK);
    d.onButtonPressed.listen((b)=> d.close() );
    return d;
  }

  /**
   * Show a confirmation dialog with YES NO buttons
   */
  static Dialog confirm(String message, [String title]){
    var d = new DialogImpl(title, message, DialogButtons.YES_NO);
    return d;
  }

  /**
   * Show a prompt dialog with a text field and OK CANCEL Buttons
   */
  static Dialog prompt(String message, [String title]){
    var d = new DialogImpl(title, message, DialogButtons.OK_CANCEL, true);
    return d;

  }

  /**
   * Sho a prompt dialog with a multiline text field (textarea) and OK CANCEL Buttons
   */
  static Dialog multiLinePrompt(String message, [String title]){
    var d = new DialogImpl(title, message, DialogButtons.OK_CANCEL, true, true);
    return d;
  }

  /**
   * Show the dialog on page
   */
  show([bool modal = false]);

  /**
   * Create a custom dialog with custom Buttons
   */
  static Dialog custom(String message, [String title, dynamic buttons]){
    var d = new DialogImpl(title, message, buttons);
    return d;
  }

  /**
   * Stream returned when some button is pressed (Clicked or touched), except the CANCEL button, for that see [Dialog.onCancel] method
   * return Stream<DialogFuture> the string representation of the button pressed with the text if applicable.
   */
  Stream<DialogFuture> get onButtonPressed;

  /**
   * Dispatched when the CANCEL button is pressed or the dialog is closed.
   */
  Future get onCancel;

  /**
   * Close the dialog removing from the dom
   */
  close();

}
class DialogButtons extends Enum<String> {
  const DialogButtons(String val): super(val);
  static const DialogButtons OK = const DialogButtons("ok");
  static const DialogButtons CANCEL = const DialogButtons("cancel");
  static const DialogButtons NO = const DialogButtons("no");
  static const DialogButtons YES = const DialogButtons("yes");
  static const DialogButtons OK_CANCEL = const DialogButtons("ok_cancel");
  static const DialogButtons YES_NO = const DialogButtons("yes_no");
  static const DialogButtons YES_NO_CANCEL = const DialogButtons("yes_no_cancel");
  @override
  toString() => value;
}
/**
 * This class represent the return from a Future when some button in the dialog is pressed
 */
class DialogFuture{
  /**
   * For default button could be *ok*, *yes*, *no*
   */
  DialogButtons button;

  /**
   * The text entered by the user if applicable, like when the dialog is a prompt
   */
  String text;

  DialogFuture([this.button, this.text]);
}