part of vade_ui;

abstract class Notify{
  void enableAnimations(bool enabled);
  bool isAnimationEnabled();
  void show(String message, {String title, Status status : Status.INFO, NotifyType type, int delay: 5000,
    Position position
  });
  void setDefaultPosition(Position position);
}
class NotifyType extends Enum<String>{
  const NotifyType(String val): super(val);
  static const NotifyType TRAY = const NotifyType('tray');
  static const NotifyType ERROR_MESSAGE = const NotifyType('error');
  static const NotifyType WARNING_MESSAGE = const NotifyType('warning');
  static const NotifyType DESKTOP = const NotifyType('desktop');
}
