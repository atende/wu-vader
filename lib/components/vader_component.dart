library vader_ui.components;
import 'dart:html';
import "package:angular/angular.dart";
import "package:dnd/dnd.dart";

part "window/window.dart";
part 'panel.dart';

class VaderComponentModule extends Module {
  VaderComponentModule(){
    bind(PanelComponent);
    install(new WindowModule());
  }

}

/**
* Initialize CSS on Page
*/
void initVaderCss() {

//initialize and load CSS
  final String _CSSID = '_vader-app-css_';
  final String _src = 'packages/vader/web/stylesheets/application.css';
  var cssElement = document.head.query('#${_CSSID}');
  if (cssElement == null){
    cssElement = new LinkElement();
    cssElement.id = _CSSID;
    cssElement.href =  _src;
    cssElement.type = "text/css";
    cssElement.rel = "stylesheet";
    document.head.append(cssElement);
  }
}

bool toBool(x) {
  if (x is bool) return x;
  if (x is num) return x != 0;
  if (x is String) return (x as String).toLowerCase() == "true";
  return false;
}

int toInt(x) {
  if (x is num) return x.toInt();
  if (x is String) return int.parse(x);
  throw new Exception("Can't translate $x to int");
}

Element compile(html, Injector injector, Compiler compiler, {Scope scope, DirectiveMap directives}) {
  List<Node> rootElements;
  if (scope != null) {
    injector = new ModuleInjector([new Module()..bind(Scope, toValue:scope)], injector);
  }
  if (html is String) {
    rootElements = toNodeList(html.trim());
  } else if (html is Node) {
    rootElements = [html];
  } else if (html is List<Node>) {
    rootElements = html;
  } else {
    throw 'Expecting: String, Node, or List<Node> got $html.';
  }
  Element rootElement = rootElements.length > 0 && rootElements[0] is Element ? rootElements[0] : null;
  if (directives == null) {
    directives = injector.get(DirectiveMap);
  }
  ViewFactory viewFactory = compiler(rootElements, directives);
  Scope _scope = scope != null ? scope : injector.get(Scope);
  DirectiveInjector directiveInjector = injector.get(DirectiveInjector);
  View rootView = viewFactory(_scope, directiveInjector, rootElements);
  return rootElement;
}

List<Element> toNodeList(html) {
  var div = new DivElement();
  div.setInnerHtml(html, treeSanitizer: new NullTreeSanitizer());
  var nodes = [];
  for(var node in div.nodes) {
    nodes.add(node);
  }
  return nodes;
}
