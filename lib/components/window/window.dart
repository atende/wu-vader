part of vader_ui.components;
/** Window Component **/
class WindowModule extends Module {
  WindowModule(){
    bind(WindowComponent);
    bind(WindowService);
  }

}

@Component(
    selector: 'v-window',
    templateUrl: 'packages/vader_ui/components/window/window.html',
//    cssUrl: 'packages/vader/components/window/window.css',
    useShadowDom: false
)
class WindowComponent extends PanelComponent implements AttachAware, DetachAware, ScopeAware {
  static const bool DEFAULT_MODAL = false;
  static const bool DEFAULT_CENTERED = true;

  bool _modal = DEFAULT_MODAL;
  bool _centered = DEFAULT_CENTERED;
  Element _element;
  Draggable _drag;
  Scope _scope;
  WindowComponent(Element element): super(element){
    this._element = element;
    this._collapsible = false;
    _drag = new Draggable(this._element, avatarHandler: new AvatarHandler.original(), handle: '.v-window-header');
  }

  @NgAttr('modal')
  set modal(String m){
   _modal = m == null? DEFAULT_MODAL : toBool(m);
  }
  get modal => _modal;
  @NgAttr('centered')
  set centered(String m){
    _centered = m == null ? DEFAULT_CENTERED : toBool(m);
  }
  get centered => _centered;
  @override
  attach(){
    if(_modal){
      _showModal();
    }
    if(_centered || _modal){
      center();
    }
  }
  @override
  detach(){
    var element = querySelector("#v-modal-element");
    if(element != null && _modal){
      element.classes.add('hide');
    }

  }
  _showModal(){

    var modalElement = querySelector('#v-modal-element');
    if(modalElement == null){
      var div = new Element.div();
      div.id = "v-modal-element";
      div.classes.add("reveal-modal-bg");
      div.style.display = 'block';
      document.body.children.add(div);
      modalElement = div;
    }
    modalElement.classes.remove('hide');
    this._element.style.setProperty('z-index', "1010");
  }
  center(){
    var host = this._element;
    // Center Horizontal
    host.style.setProperty("left", "50%");
    int width = host.offsetWidth;
    double marginLeft = width/2;
    host.style.setProperty("margin-left", "-${marginLeft}px");
    // Center Vertical
    host.style.setProperty("top", "50%");
    int height = host.offsetHeight;
    double marginTop = height/2;
    host.style.setProperty("margin-top", "-${marginTop}px");

  }

  void set scope(Scope scope) {
    _scope = scope;
  }
  @override
  close(){
    _scope.emit('window-closed');
    _element.remove();
    detach();
  }
}
@Injectable()
class WindowService {
  Compiler _compiler;
  Injector _injector;
  DirectiveMap _directiveMap;
  Scope _scope;
  DirectiveInjector _directiveInjector;
  Element element;
  WindowService(this._compiler, this._injector, this._directiveMap, this._scope, this._directiveInjector);

  Element open(WindowOptions options, Scope scope){
    assert(options != null);
    return _show(options, scope);
  }
  close(){
    _scope.emit('window-closed');
    element.remove();
  }
  Element _show(WindowOptions options, Scope scope){
    var win = new Element.tag('v-window');
    String html = "<div class='content'>";
    if(options.innerHtml != null)
      html += options.innerHtml;
    html += "</div>";
    win.setInnerHtml(html, treeSanitizer: new NullTreeSanitizer());

    if(options.buttons != null && !options.buttons.isEmpty) {
      DivElement div = new Element.div();
      div.classes.add("buttons");
      options.buttons.forEach((b)=> div.children.add(b));
      win.children.add(div);
    }
    if(options.title != null)
      win.attributes['title'] = options.title;
    if(options.modal){
      win.attributes['modal'] = "true";
    }
    if(options.center){
      win.attributes['centered'] = "true";
    }
    if(options.collapse){
      win.attributes['collapsible'] = "true";
    }else {
      win.attributes['collapsible'] = "false";
    }
    if(options.close) {
      win.attributes['closable'] = "true";
    }else {
      win.attributes['closable'] = "false";
    }
    element = compile(win,_injector,_compiler, scope: _scope, directives: _directiveMap);
    document.body.children.add(element);
    return win;
  }
}
class WindowOptions {
  bool modal;
  bool center;
  bool collapse;
  bool close;
  List<ButtonElement> buttons;
  String title;
  String innerHtml;

  WindowOptions({this.modal: false, this.title, this.innerHtml, this.center: true, this.collapse: true, this.close: true, this.buttons});

}