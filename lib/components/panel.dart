part of vader_ui.components;
@Component(
  selector: 'v-panel',
  templateUrl: 'packages/vader_ui/components/panel.html',
  useShadowDom: false
)
class PanelComponent {
   static const DEFAULT_COLLAPSIBLE = true;
   static const DEFAULT_CLOSABLE = true;
   @NgAttr('title')
   String title;

   bool _collapsible = DEFAULT_COLLAPSIBLE;

   bool _closable = DEFAULT_CLOSABLE;

   Element _element;
   PanelComponent(this._element);
   @NgAttr('collapsible')
   set collapsible(String value){
     _collapsible = value == null ? DEFAULT_COLLAPSIBLE : toBool(value);
   }
   get collapsible => _collapsible;

   @NgAttr('closable')
   set closable(String m){
     _closable = m == null ? DEFAULT_CLOSABLE : toBool(m);
   }
   get closable => _closable;

   bool doCollapse = false;

  void close(){
    _element.remove();
  }
  void collapse(){
     doCollapse = !doCollapse;
  }


}