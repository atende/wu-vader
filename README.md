Dart Vader User Interface
==========================

[![Build Status](https://drone.io/bitbucket.org/atende/wu-vader/status.png)](https://drone.io/bitbucket.org/atende/wu-vader/latest)
Vader is a component library and tools for User Interface Programming using Angular component architecture.

This library is in development, use with care. [See the source code][source]

## Installing

Follow instructions on [pub page][pub]
You will need some css, for the components, you must include the follow CSS then in your html file:


    <link rel="stylesheet" href="packages/vader/web/stylesheets/application.css" />
    <!-- Some Animations use the animate.css see: http://daneden.github.io/animate.css/ -->
    <link rel="stylesheet" href="packages/vader/web/bower_components/animate.css/animate.min.css" />
    <!-- Icons -->
    <link rel="stylesheet" href="packages/vader/web/fonts.css" />


## API documentation

By this date there is no API documentation, the API is changing. But you can look in the source code.
You will see a *api* and a *src* folder. We try split the *api* from the implementation where it is possible.

## Running the examples

In this early versions, the best way to learn is by the examples. The documentation is not good right now.

You need compile the CSS using *sass* and download the dependencies like *jquery* and *foundation sass* using [bower][bower].
For this purpose follow the commands, after download the source code

    cd vader/lib/web
    bower install
    compass compile
    cd ../../
    pub install

To run the examples download the [source code][source] enter in **example** folder and use the command ```pub serve```.

[pub]: http://pub.dartlang.org/packages/vader#installing
[docs]: https://atende.atlassian.net/wiki/display/VADER
[bower]: http://bower.io
[source]: https://bitbucket.org/atende/wu-vader