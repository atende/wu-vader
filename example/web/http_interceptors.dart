part of vader_examples;
/**
 * Interceptor example, this just forward the logic for the VaderHttpInterceptor.
 * Since VaderHttpInterceptor do **NOT** depends on Angular
 */
class ExampleHttpInterceptor implements HttpInterceptor {
  VaderHttpInterceptor impl;

  ExampleHttpInterceptor(VaderHttpInterceptor v){
    impl = v;
  }
  responseError(error) {
    impl.responseError(error.responseText, error.status);
    return new Future.error(error);
  }
  request(HttpResponseConfig config){
    impl.processRequest(config.data);
    return config;
  }
  requestError(request){
    return new Future.error(request);
  }
  response(HttpResponse httpResponse){
    impl.processRequest(httpResponse.data);
    return httpResponse;
  }
}