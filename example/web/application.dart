library vader_examples;
import 'package:angular/angular.dart';
import 'package:angular/application_factory.dart';
import 'dart:html';
import 'dart:async';
import 'package:vader_ui/vader.dart';
import 'package:vader_ui/components/vader_component.dart';
import 'package:logging/logging.dart';


part 'controllers/notify_tray_controller.dart';
part 'route.dart';
part 'http_interceptors.dart';
part 'controllers/error_warning_controller.dart';
part 'controllers/http_interceptor_controller.dart';
part 'controllers/notify_desktop.dart';
part 'controllers/window_controller.dart';
part 'controllers/panel_controller.dart';

main(){
  print('Initilizing...');
  // Setup Logger
  Logger logger = new Logger('examples');
  var module = new Module()
    ..install(new VaderModule())
    ..bind(RouteInitializerFn, toImplementation: ExampleRouterInitializerFn)
    ..bind(NgRoutingUsePushState, toValue: new NgRoutingUsePushState.value(false))
    ..bind(NotifyController)
    ..bind(ErrorWarningController)
    ..bind(HttpInterceptorController)
    ..bind(WindowController)
    ..bind(NotifyDesktopController)
    ..bind(PanelCtrl);

  /**
   * This is important because Vader need access to angular facilities to compile components without injection,
   * and this is only available after application bootstrap
   * This object is used in compileAngularElement function
   */
  di = applicationFactory().addModule(module).run();
  // Setup Interceptors
  VaderHttpInterceptor vaderInterceptors = di.get(VaderHttpInterceptor);
  di.get(HttpInterceptors).add(new ExampleHttpInterceptor(vaderInterceptors));
  print('Application Initialized');
}