part of vader_examples;
@Component(
 selector: '[window-example]',
 templateUrl: 'controllers/window_controller.html',
 useShadowDom: false
)
class WindowController implements ScopeAware {
  WindowService windowService;
  Notify _notify;
  Scope _scope;
  WindowController(this.windowService, this._notify);

  bool modal = false;
  show(){
    WindowOptions op = new WindowOptions(modal: modal, title:"Titulo", innerHtml: "<p>Olá <b>Mundo</b></p>", center: true);
    windowService.open(op, _scope);
  }
  showOK(){
    var d = Dialog.alert("This is a dialog with a OK button");
      d.onButtonPressed.listen((b){
        _notify.show("Ok button was pressed");
        d.close();
      });
      d.show();
  }
  showOKCANCEL(){
    var d = Dialog.custom("Ok Cancel Dialog", null, DialogButtons.OK_CANCEL);
    d.onButtonPressed.listen((b){
      if(b.button == DialogButtons.OK){
        _notify.show("Ok Button pressed");
      }else if(b.button == DialogButtons.CANCEL){
        _notify.show("Cancel button pressed");
      }
    });
    d.show();
  }

  showYESNOCANCEL(){
    var d = Dialog.custom("Yes No Cancel", null, DialogButtons.YES_NO_CANCEL);
    d.onButtonPressed.listen((b){
      if(b.button == DialogButtons.YES){
        _notify.show("Yes button pressed");
      }else if(b.button == DialogButtons.NO){
        _notify.show("No button pressed");
      }else if(b.button == DialogButtons.CANCEL){
        _notify.show("Cancel button pressed");
      }
    });
    d.show();
  }
  confirm() {
    var d = Dialog.confirm("Can I destroy the world?", "Please confirm");
    d.onButtonPressed.listen((b){
      if(b.button == DialogButtons.YES){
        _notify.show("Destroing the world!", status: Status.SUCCESS);
      }else if(b.button == DialogButtons.NO){
        _notify.show("Sorry a can't destroy de world without your permission", status: Status.WARNING);
      }

    });
    d.show(true);

  }

  alert() {
    var d = Dialog.alert("This is a alert","Title");
    d.onButtonPressed.listen((b)=> _notify.show("Alert window ok pressed"));
    d.onCancel.then((_)=>_notify.show("The alert was canceled or closed", status: Status.WARNING));
    d.show(true);
  }

  prompt(){
    var d = Dialog.prompt("Enter your name");
    d.onButtonPressed.listen((b){
      _notify.show("You entered ${b.text}");
      d.close();
    });
    d.show();
  }
  multiline(){
    var d = Dialog.multiLinePrompt("Enter your name");
    d.onButtonPressed.listen((b){
      _notify.show("You entered ${b.text}");
      d.close();
    });
    d.show();
  }

  customButtons(){
    var customB1 = new ButtonElement();
    customB1.innerHtml = "Oh men!";
    var customB2 = new ButtonElement();
    customB2.innerHtml = "What?";
    customB1.onMouseEnter.listen((e)=>customB1.innerHtml = "Click me and I will close");
    customB1.onMouseLeave.listen((e)=>customB1.innerHtml = "Oh men!");

    var d = Dialog.custom("This has custom buttons", null, [customB1, customB2]);
    customB1.onClick.listen((e)=> d.close());
    customB2.onClick.listen((e)=> _notify.show("""I'm a "what? button".
    <br /> I dont know what to do, but I know how to notify!
    <br /> By the way, <b>click me to dismiss</b>""", delay: 0));
    d.show();
  }

  void set scope(Scope scope) {
    _scope = scope;
  }

}