part of vader_examples;
@Component(
  selector: '[error-warning]',
  templateUrl: 'controllers/error_warning_controller.html',
  useShadowDom: false
)
class ErrorWarningController {
  Notify n;
  ErrorWarningController(this.n);
  error(){
    n.show("This is a error message", title: "Sorry man", type: NotifyType.ERROR_MESSAGE);
  }
  warning(){
    n.show("Warning is the message", title: "What? pay attention", type: NotifyType.WARNING_MESSAGE);
  }
}