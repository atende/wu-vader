part of vader_examples;
@Component(
  selector: '[tray-notification]',
  useShadowDom: false,
  templateUrl: 'controllers/notify_tray_controller.html'
)
class NotifyController {
  Notify notification;
  NotifyController(this.notification){
    window.console.debug('Notify Controller Started');
  }
  void sendNotification(){
    notification.show("Default notification", title: "Default");
    notification.show("Info Notification", title: "Info", status: Status.INFO);
    notification.show("Success Notification", title: "Success", status: Status.SUCCESS);
    notification.show("Warning Notification", title: "Warning",status: Status.WARNING);
    notification.show("Error Notification", title: "Error", status: Status.ERROR);
  }
  void defaultNotification(){
    notification.show("Default Notification", title: "Default");
  }
  void disappear(){
    notification.show("This message will disappear in 10 seconds", status: Status.SUCCESS, delay: 10000);
  }
  void clickToClose(){
    notification.show("You need to click in this notification to close it", delay: 0, status: Status.WARNING);
  }
  void noAnimation(){
    var message = notification.isAnimationEnabled() ?
      "Animations was <b>enabled</b>, after this message, animation is <b>disabled</b>"
      : "Animations was <b>disabled</b>, after this message, animation is <b>enabled</b>";
    notification.enableAnimations(notification.isAnimationEnabled()? false : true);
    notification.show(message, delay: 10, status: Status.WARNING);

  }
  topLeft(){
    notification.show("This notifications is on top-left", position: Position.TOP_LEFT);
  }
  topCenter(){
    notification.show("This notifications is on top-center", position: Position.TOP_CENTER);
  }
  topRight(){
    notification.show("This notifications is on top-right", position: Position.TOP_RIGHT);
  }
  bottomLeft(){
    notification.show("This notifications is on bottom-left", position: Position.BOTTOM_LEFT);
  }
  bottomCenter(){
    notification.show("This notifications is on bottom-center", position: Position.BOTTOM_CENTER);
  }

  bottomRight(){
    notification.show("This notifications is on bottom-right", position: Position.BOTTOM_RIGHT);
  }
  changeDefaultPosition(){
    notification.setDefaultPosition(Position.BOTTOM_RIGHT);
  }
}