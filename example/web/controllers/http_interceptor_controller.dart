part of vader_examples;
@Component(
  selector: '[http-interceptor]',
  templateUrl: 'controllers/http_interceptor_controller.html',
  useShadowDom: false
)
class HttpInterceptorController {
  Http _http;
  HttpInterceptorController(this._http);
  sendNotFound(){
    _http.get('data/notfound');
  }
  sendError(){
    _http.get('data/error.json');
  }
  sendOk(){
    _http.get('data/ok.json');
  }
}