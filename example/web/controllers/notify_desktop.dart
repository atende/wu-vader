part of vader_examples;
@Component(
    selector: '[desktop-notification]',
    templateUrl: 'controllers/notify_desktop.html',
    useShadowDom: false
)
class NotifyDesktopController {
  Notify n;
  NotifyDesktopController(this.n);
  sayHello(){
    n.show("This is a notification in the desktop", title: 'Title', type: NotifyType.DESKTOP);
  }
}
