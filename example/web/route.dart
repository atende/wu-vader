part of vader_examples;
@Injectable()
class ExampleRouterInitializerFn {
  void call(Router router, RouteViewFactory views) {
    views.configure({
        'default': ngRoute(
            path: '/',
            defaultRoute: true, //            preEnter: (RoutePreEnterEvent e) => e.allowEnter(this._userService.requireUserState(UserService.LOGGED_IN)),
            view: 'views/getting-started.html'
        ),
        'notification': ngRoute(
            path: 'notification',
            mount: {
              'tray': ngRoute(
                path: '/tray',
                view: 'views/notification/tray.html'
              ),
              'error-warning': ngRoute(
                path: '/error-warning',
                view: 'views/notification/error-warning.html'
              ),
              'desktop': ngRoute(
                path: '/desktop',
                view: 'views/notification/desktop.html'
              )
            }
        ),
        'http_interceptor': ngRoute(
          path: 'http-interceptor',
          view: 'views/http_interceptor.html'
        ),
        'panels': ngRoute(
          path: 'panel',
          view: 'views/panel.html'
        ),
        'window': ngRoute(
          path: 'window',
          view: 'views/window.html'
        )
    });
  }
}