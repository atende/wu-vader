library vader.notification;
import 'package:vader_ui/vader.dart';
import '../_specs.dart';
import 'dart:async';
main(){
  TestBed _;
  Scope _scope;
  Element ngAppElement;
  Notify notification;
  beforeEach(async(inject((TestBed tb, Scope scope, VmTurnZone zone, Notify notify) {
    _ = tb;
    _scope = scope;
    notification = notify;
  })));
  beforeEachModule((Module module) {
    ngAppElement = new DivElement()..attributes['ng-app'] = '';
    module
      ..install(new VaderModule());
    module..bind(Node, toValue: ngAppElement);
    document.body.append(ngAppElement);
  });


  describe('Notification',(){
    it('should create a default tray notification', async((){
      notification.enableAnimations(false);
      // Run notification and expect to apperar in browser
      notification.show("Message", delay: 0);
      var ndom = querySelector("#v-notifications-tray");
      expect(ndom,isNotNull);
      expect(ndom.children.length, greaterThan(0));
      expect(ndom.children.first.classes.contains('alert-box'), isTrue);
      expect(ndom.children.first.classes.contains('v-notification'), isTrue);
      expect(ndom.children.first.classes.contains('v-notification-tray'), isTrue);
      expect(ndom.children.first.innerHtml.contains('<p>Message</p>'), isTrue);
      expect(ndom.clientTop, equals(0));

      var firstNotification = ndom.children.first;
      firstNotification.click();
      expect(ndom.children.length, equals(0));
      var nf = querySelector(".alert-box");
      expect(nf, isNull);
      notification.show("Warning",status: Status.WARNING, delay: 0);
      expect(ndom.children.length, equals(1));
      firstNotification = ndom.children.first;
      expect(firstNotification.classes.contains('warning'), isTrue);
      ndom.children.clear();
      notification.show('',status: Status.ERROR, delay: 0);
      expect(ndom.children.first.classes.contains('alert'), isTrue);
      ndom.children.clear();
      notification.show('',status: Status.INFO, delay: 0);
      expect(ndom.children.first.classes.contains('info'), isTrue);
      ndom.children.clear();
      notification.show('',status: Status.SUCCESS, delay: 0);
      expect(ndom.children.first.classes.contains('success'), isTrue);
    }));
    it('should create a error div for error message and reuse this',(){
      notification.show("Message", type: NotifyType.ERROR_MESSAGE);
      var ndom = querySelector("#v-notification-error-message");
      expect(ndom, isNotNull);
    });
    it('should create a warning div for warning messages with the warning',(){
      notification.show("Message", type: NotifyType.WARNING_MESSAGE, delay: 0);
      var ndom = querySelector("#v-notification-warning-message");
      expect(ndom, isNotNull);
      expect(ndom.classes.contains('warning-message'), isTrue);
      expect(ndom.classes.contains('v-notification-warning-message'), isTrue);
    });
    it('should close error message on click removing from the dom',(){
      notification.enableAnimations(false);
      var ndom = querySelector("#v-notification-error-message");
      if(ndom != null){
        ndom.remove();
      }
      expect(document.contains(ndom)).toBeFalse();
      notification.show("Message", type: NotifyType.ERROR_MESSAGE, delay: 0);
      ndom = querySelector("#v-notification-error-message");
      ndom.click();

      expect(document.contains(ndom)).toBeTrue();
      ndom.children.first.click();
      ndom = querySelector("#v-notification-error-message");
      expect(document.contains(ndom)).toBeFalse();
    });
    it('should close warning message removing from the dom',(){
      notification.enableAnimations(false);
      // Ensure warning message does not exists
      var ndom = querySelector("#v-notification-warning-message");
      if(ndom != null){
        ndom.remove();
      }
      notification.show("Message", type: NotifyType.WARNING_MESSAGE, delay: 0);
      ndom = querySelector("#v-notification-warning-message");
      ndom.click();

      expect(document.contains(ndom), isTrue);
      ndom.children.first.click();
      ndom = querySelector("#v-notification-warning-message");
      expect(document.contains(ndom), isFalse);
    });

    it('should close tray messages removing from the dom', async((){
      // Ensure that notifications is empty for the test
      notification.enableAnimations(false);
      var ndom = querySelector("#v-notifications-tray");
      if(ndom != null){
        ndom.children.clear();
      }

      notification.show("Bla", delay: 100);

      clockTick(milliseconds: 150);

      ndom = querySelector("#v-notifications-tray");
      expect(ndom.children.length, equals(0));

    }));
    it('should close erro message automatically with a delay', async((){
      // Ensure that notifications is empty for the test
      var ndom = querySelector("#v-notification-error-message");
      if(ndom !=null){
        ndom.remove();
      }
      notification.show("", type: NotifyType.ERROR_MESSAGE, delay: 100);
      _test(){
        var ndom = querySelector("#v-notification-error-message");
        expect(ndom, isNotNull);
        expect(ndom.children.length, greaterThan(0));
      }
      clockTick(milliseconds: 110);
      _test();
    }));

  });
}