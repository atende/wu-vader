library vader.http_interceptor;
import '../_specs.dart' hide Logger;
import "package:vader_ui/vader.dart";
import "package:vader_ui/src/http/http_interceptors_impl.dart";
import "dart:convert";
import "package:logging/logging.dart";

class NotifyMock implements Notify {
  String title;
  String message;
  Status status;
  int delay;
  NotifyType type;
  void show(String message, {String title, Status status : Status.INFO, NotifyType type, int delay: 5000,
  Position position
  }){
    this.title = title;
    this.message = message;
    this.status = status;
    this.type = type;
    this.delay = delay;
  }
  enableAnimations(enabled){

  }
}

main(){
  describe('Http Interceptors',(){
    Logger logger = new Logger("Test For Http");
    var notifyMock = new NotifyMock();
    VaderHttpInterceptor exception = new VaderHttpInterceptorImpl(logger, notifyMock);
    it('should error message',(){

        exception.responseError("Eu sou a mosca que pousou na sua sopa", 501);
        expect(notifyMock.message, "Eu sou a mosca que pousou na sua sopa");
        expect(notifyMock.type, NotifyType.ERROR_MESSAGE);
        exception.responseError("Eu sou a mosca que pousou na sua sopa", 404);
        expect(notifyMock.type, NotifyType.WARNING_MESSAGE);
        // To not interfer in others tests, set the mock to the default
      });
    it('should proccess information correctly',(){
        notifyMock = new NotifyMock();
        exception = new VaderHttpInterceptorImpl(logger, notifyMock);
        var request = {'map': 'Not contain information'};
        exception.processRequest(request);
        expect(notifyMock.message, isNull);
        request = {'success': true, 'message': 'Message'};
        exception.processRequest(request);
        expect(notifyMock.message, 'Message');
        expect(notifyMock.status, Status.INFO);
        expect(notifyMock.title, isNull);

        request = {'success': false, 'message': 'Message2'};
        exception.processRequest(request);
        expect(notifyMock.message, 'Message2');
        expect(notifyMock.status, Status.WARNING);

        notifyMock.message = null;
        request = JSON.encode({'success': true, 'message': 'Message Json'});
        exception.processRequest(request);
        expect(notifyMock.message, 'Message Json');

    });
  });
}
