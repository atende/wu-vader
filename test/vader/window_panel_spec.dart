library vader.panel_spec;
import '../_specs.dart';
import "package:vader_ui/components/vader_component.dart";

  main(){
    describe('Tests for Panel UI',(){
      TestBed _;
      Scope _scope;
      Element ngAppElement;
      beforeEach(async(inject((TestBed tb, Scope scope, VmTurnZone zone, TemplateCache cache) {
        _ = tb;
        _scope = scope;
        addToTemplateCache(cache, 'packages/vader_ui/components/panel.html');
        addToTemplateCache(cache, 'packages/vader_ui/components/window/window.html');

      })));
      beforeEachModule((Module module) {
        ngAppElement = new DivElement()..attributes['ng-app'] = '';
        module
          ..install(new VaderComponentModule());
        module..bind(Node, toValue: ngAppElement);
        document.body.append(ngAppElement);
      });
      afterEach(() {
        ngAppElement.remove();
        ngAppElement = null;
      });
      compile(html) {
        ngAppElement.setInnerHtml(html, treeSanitizer: new NullTreeSanitizer());
        _.compile(ngAppElement);
        return ngAppElement.firstChild;
      }
      it("should create a panel with title and other informations",async((){
        compile("<v-panel id='panel' title='Titulo'></v-panel>");
        microLeap();
        _.rootScope.apply();
        var panel = querySelector("#panel");
        expect(panel.querySelector('.title').innerHtml).toContain('<h5 class="ng-binding">Titulo</h5>');
        expect(panel.querySelector("#collapse-icon"), isNotNull);
        expect(panel.querySelector("#close-icon"), isNotNull);
      }));
      it("should create a panel without collapse and close",async((){
        compile("<v-panel id='panel' collapsible='false' closable='false'></v-panel>");
        microLeap();
        _.rootScope.apply();
        var panel = querySelector("#panel");
        expect(panel.querySelector("#collapse-icon"), isNull);
        expect(panel.querySelector("#close-icon"), isNull);
      }));
      it("should collapse when clicked", async((){
        Element panel = compile("<v-panel>CollapsedItem</v-panel>");
        microLeap();
        _.rootScope.apply();
        Element contentPanel = panel.querySelector('.content-panel');
        expect(contentPanel.classes.contains('collapse'), isFalse);
        _.triggerEvent(panel.querySelector('#collapse-icon'), 'click');
        _.rootScope.apply();
        var comp = ngInjector(panel).get(PanelComponent);
        expect(comp.doCollapse, isTrue);
      }));
      it("should remove when clicked", async((){
        Element panel = compile("<v-panel>CollapsedItem</v-panel>");
        microLeap();
        _.rootScope.apply();
        panel = querySelector('v-panel');
        expect(panel, isNotNull);
        _.triggerEvent(panel.querySelector('#close-icon'), 'click');
        panel = querySelector('v-panel');
        expect(panel, isNull);
      }));
      it("window in modal should be created and destructed correctly",async((){

        // It must be not window to this test work
        var elements = querySelectorAll("v-window");
        elements.forEach((e)=> e.remove());

        Element dom = compile("<v-window modal='true'></v-window>");

        microLeap();
        _.rootScope.apply();
        var modal = querySelector("#v-modal-element");
        var win = querySelector("v-window");
        expect(win).toBeNotNull();
        expect(modal).toBeNotNull();
        dom.remove();
        WindowComponent comp = ngInjector(win).get(WindowComponent);
        // If detach is called modal should be hide
        comp.detach();
        modal = querySelector("#v-modal-element");
        win = querySelector("v-window");
        expect(modal).toHaveClass('hide');
        expect(win).toBeNull();
      }));
      it("window service should create window with the correcty parameters",async((WindowService _ws){
        _ws.open(new WindowOptions(title: "Titulo", innerHtml: "Mensagem"), _scope);
        var window = querySelector("v-window");
        expect(window, isNotNull);
        expect(window.attributes["title"], "Titulo");
        expect(window.innerHtml).toContain("Mensagem");
        // Clean dom
        window.remove();
      }));
    });

  }
