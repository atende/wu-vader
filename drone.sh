#!/bin/bash

set -e

export DARTIUM_DIST=dartium-linux-x64-release.zip
export DARTIUM_PATH=/usr/local/dartium

if [ -d ${DARTIUM_PATH} ]; then
 echo "Dartium is downloaded"
else
  echo "Fetching Dartium"
  wget http://storage.googleapis.com/dart-archive/channels/stable/raw/latest/dartium/$DARTIUM_DIST  --quiet
  unzip $DARTIUM_DIST > /dev/null
  rm $DARTIUM_DIST
  sudo mv dartium-* ${DARTIUM_PATH}
fi

export DARTIUM_BIN=${DARTIUM_PATH}/chrome

echo "Pub install"
pub install
echo "NPM install"
npm install

sudo start xvfb

./node_modules/karma/bin/karma start --single-run --browsers Dartium